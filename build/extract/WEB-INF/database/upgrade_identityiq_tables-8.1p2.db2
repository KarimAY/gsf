--
-- This script contains DDL statements to upgrade a database schema to
-- reflect changes to the model.  This file should only be used to
-- upgrade from the last formal release version to the current code base.
--

CONNECT TO iiq;

-- Add TaskResult.live column
alter table identityiq.spt_task_result add live smallint default 0;
update identityiq.spt_task_result set live = 0;

alter table identityiq.spt_role_change_event add status varchar(255);
alter table identityiq.spt_role_change_event add failed_identity_ids clob(100000000);
alter table identityiq.spt_role_change_event add skipped_identity_ids clob(100000000);
alter table identityiq.spt_role_change_event add affected_identity_count integer default 0 NOT NULL;
alter table identityiq.spt_role_change_event add run_count integer default 0 NOT NULL;
alter table identityiq.spt_role_change_event add failed_attempts integer default 0 NOT NULL;

--
-- This is necessary to maintain the schema version. DO NOT REMOVE.
--
update identityiq.spt_database_version set schema_version = '8.1-10' where name = 'main';
