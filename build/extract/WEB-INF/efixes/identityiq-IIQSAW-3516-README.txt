README for Cross Site Request Forgery (CSRF) via Path Traversal Fix

Security Fix Deliverable:
 
  identityiq-<versions>-IIQSAW-3516.zip


General Information:
 
  As part of SailPoint's ongoing commitment to the security of our solutions, 
  our Engineering team is releasing a security fix to address a critical 
  vulnerability in current releases of IdentityIQ.
 
  This vulnerability is a Cross Site Request Forgery (CSRF) vulnerability 
  related to CSRF protections not being applied to all required URL paths. 

  The fix for this vulnerability has general applicability and should be 
  applied to all instances of IdentityIQ.
 
  The fix includes product code change packaged in e-fix format and
  requires deploying this change to, and restart of, all application
  server instances in the IdentityIQ installation.
 
  Note that this security fix does not supersede any of the previous
  vulnerability security fixes. Updating your environment should include
  a combination of patches and/or security fixes that includes fixes for
  all known vulnerabilities. The IdentityIQ Security Vulnerabilities
  page on Compass can be used to determine which security fixes are
  required for each supported IdentityIQ version.
 
  As with all software vulnerabilities, we recommend that you apply this
  security fix or a patch containing resolution to this issue as soon as
  reasonably possible.

  CVSS: 8
 

Installation Instructions:
 
  1. Download the security fix file for the version of IdentityIQ in
     use. The security fix is compatible with all patch levels of the
     indicated version unless otherwise specified.
 
  2. Extract the security fix into the root of each IdentityIQ instance in the
     IdentityIQ installation.
 
     Alternatively, use site processes and procedures for introducing
     security fixes into the deployable build process and for deploying
     IdentityIQ to application server instances.
 
  3. Restart all application server instances in the IdentityIQ installation.

 
Installation Verification:
 
  Steps to verify:

    1. Login to IdentityIQ as a user with the System Administrator capability

    2. Navigate to the Debug page (/debug/debug.jsf)

    3. Navigate to the wrench icon -> About

    4. Verify that the EFIX appears under the Product Information

  For more detailed instructions on how to verify the installation of this
  security fix, contact your Customer Success Manager.


Security Fix Contents:
 
  This e-fix package includes a list of the security fix contents in a manifest
  file named:

     WEB-INF/efixes/identityiq-<versions>-IIQSAW-3516.txt
